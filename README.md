# Bluetooth Receivers

This shoud be a centralized place for information about USB bluetooth receivers. Probably I am the only one who cares about them but anyway feel free to contribute :)

Learn more about these receivers:
- [BT-163](BT-163/details.md)
- [YET-M1](YET-M1/details.md)
- [Bluetooth music](Bluetooth_music/details.md) (Yes thats the Name)

Also here is an [article](https://goughlui.com/2019/11/17/tested-generic-usb-bluetooth-receivers-pix-lv-b02l-h7x-rx-163/) about in depth testing


### What is this?
These devices are receivers of a bluetooth audio stream that will be forwarded via audio jack. The USB is ONLY for power and can not be used as audio output. On many webstores they are available for cheap prices down to 1,50€


### Hacking the device

https://www.youtube.com/watch?v=6VUQo5sFOYk
